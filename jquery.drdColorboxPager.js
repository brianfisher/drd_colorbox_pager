(function ($) {

  // cached objects
  var
    $pagers = {},
    $pages = {};

  $.fn.drdColorboxPager = function(options) {

    var settings = {
      pager: ".colorbox-pager",   // selector for pager container
      n: -1,                      // number of page numbers to show on each side
                                  // of current page number. If 0, no numbers
                                  // are shown.
      ellipses: '&hellip;',       // string to indicate missing page numbers
      first: '&laquo; first',     // string for first link
      prev: '&lsaquo; previous',  // string for previous link
      next: 'next &rsaquo;',      // string for next link
      last: 'last &raquo;',       // string for last link
      colorbox: false             // colorbox settings
    };
    $.extend(settings, options);

    // render pager
    var pager = function(parent) {
      var item;
      var ul = $('<ul>').addClass('pager clearfix');
      parent.html($('<div class="item-list">').append(ul));
      var pages = $pages[settings.colorbox.rel];

      // first
      if (settings.first) {
        item = $('<a>')
          .attr('href', '#drd-colorbox-page-0')
          .html(settings.first)
          .click([0], show);
        item = $('<li>')
          .addClass('pager-first')
          .append(item);
        ul.append(item);
      }

      // previous
      if (settings.prev) {
        item = $('<a>')
          .html(settings.prev);
        item = $('<li>')
          .addClass('pager-previous')
          .append(item);
        ul.append(item);
      }

      // ellipses
      if (settings.ellipses && settings.n > 0) {
        item = $('<span>' + settings.ellipses + '</span>');
        item = $('<li>')
          .addClass('ellipses-previous')
          .append(item);
        ul.append(item);
      }

      // numbers
      if (settings.n !== 0) {
        for (var i = 0; i < pages.length; i++) {
          item = $('<span>' + (i + 1) + '</span>');
          item = $('<a>')
            .attr('href', '#drd-colorbox-page-' + i)
            .html(item)
            .click([i], show);
          item = $('<li>')
            .addClass('pager-item')
            .addClass('page-' + i)
            .append(item);
          ul.append(item);
        }
      }

      // ellipses
      if (settings.ellipses && settings.n > 0) {
        item = $('<span>' + settings.ellipses + '</span>');
        item = $('<li>')
          .addClass('ellipses-next')
          .append(item);
        ul.append(item);
      }

      // next
      if (settings.next) {
        item = $('<a>')
          .html(settings.next);
        item = $('<li>')
          .addClass('pager-next')
          .append(item);
        ul.append(item);
      }

      // last
      if (settings.last) {
        item = $('<a>')
          .attr('href', '#drd-colorbox-page-' + (pages.length - 1))
          .html(settings.last)
          .click([pages.length - 1], show);
        item = $('<li>')
          .addClass('pager-last')
          .append(item);
        ul.append(item);
      }
    };

    // show a page
    var show = function(event) {
      var pages = $pages[settings.colorbox.rel];
      var pagers = $pagers[settings.colorbox.rel];
      // current page
      var current = event.data[0];
      // show current page
      pages.hide();
      pages.filter('.drd-colorbox-page.page-' + current).show();
      // min/max numbers
      var num_start = 0;
      var num_end = pages.length;
      if (settings.n > -1) {
        num_start = current - settings.n;
        if (num_start < 0) num_start = 0;
        num_end = current + settings.n;
        if (num_end >= pages.length)
          num_end = pages.length - 1;
      }
      // first
      pagers.find('li.pager-first').toggle(current !== 0);
      // previous
      pagers.find('li.pager-previous').toggle(current !== 0)
        .find('a')
          .attr('href', '#drd-colorbox-page-' + (current - 1))
          .click([current - 1], show);
      // ellipses
      pagers.find('li.ellipses-previous').toggle(num_start !== 0);
      // numbers
      pagers.find('.pager-item').each(function(i) {
        $(this)
          .toggle(num_start <= i && i <= num_end)
          .toggleClass('active', i == current)
          .find('a').toggleClass('active', i == current);

      });
      // ellipses
      pagers.find('li.ellipses-next').toggle(num_end !== pages.length - 1);
      // next
      pagers.find('li.pager-next').toggle(current !== pages.length - 1)
        .find('a').
          attr('href', '#drd-colorbox-page-' + (current + 1))
          .click([current + 1], show);
      // last
      pagers.find('li.pager-last').toggle(current !== pages.length - 1);

      // parent.find('ul li:first-child').addClass('first');
      // parent.find('ul li:last-child').addClass('last');
    };

    if (!settings.colorbox || !settings.colorbox.rel) return;

    var $this = $(this);

    var initialized = true;
    if (!$pagers[settings.colorbox.rel]) {
      initialized = false;
      $pagers[settings.colorbox.rel] = $(settings.pager);
      $pages[settings.colorbox.rel] = $this
        .parents(".drd-colorbox-page")
        .parent()
        .find(".drd-colorbox-page");
    }

    $this.colorbox(settings.colorbox);

    // done if pager already exists
    if (initialized) return;

    // need a pager?
    if ($pages[settings.colorbox.rel].length < 2) return $this;

    // render
    $pagers[settings.colorbox.rel].each(function(i) { pager($(this)); });

    var anchor = self.document.location.hash.substring(1);
    var page = anchor.match(/^drd-colorbox-page-([0-9]+)$/) ?
      parseInt(RegExp.$1, 10) : 0;
    show({data: [page]});

    return $this;

  };

})(jQuery);
