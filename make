#!/bin/sh

# https://developers.google.com/closure/compiler/docs/api-ref

curl -d output_info=compiled_code -d compilation_level=SIMPLE_OPTIMIZATIONS --data-urlencode js_code@jquery.drdColorboxPager.js http://closure-compiler.appspot.com/compile > jquery.drdColorboxPager.min.js
